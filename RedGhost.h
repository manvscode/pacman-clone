#pragma once
#ifndef _REDGHOST_H_
#define _REDGHOST_H_
#include "Ghost.h"



class RedGhost : public Ghost
{
  protected:

  public:
	RedGhost( );
	~RedGhost( );

	void render( );
	void update( );

};

#endif