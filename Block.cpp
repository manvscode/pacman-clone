#include "Block.h"
#include "PacManGame.h"

Block::Block( )
{
	type = OBJ_BLOCK;
}

Block::~Block( )
{
}

void Block::render( )
{
	glPushAttrib( GL_TEXTURE_BIT );
	glPushMatrix( );
		glTranslatef( X * Game::BLOCK_WIDTH, Y * Game::BLOCK_HEIGHT, 0 );
		glBindTexture( GL_TEXTURE_2D, Game::nBlockTexture[ Game::nLevel % 7 ] );
		glCallList( Object::m_DisplayList );
	glPopMatrix( );
	glPopAttrib( );
}

void Block::update( )
{
	// do nothing
}