#include "SmallDot.h"
#include "PacManGame.h"

SmallDot::SmallDot( )
{
	type = OBJ_SMALL_DOT;
}

SmallDot::~SmallDot( )
{
}

void SmallDot::render( )
{
	glPushAttrib( GL_TEXTURE_BIT );
	glPushMatrix( );
		glTranslatef( X * Game::BLOCK_WIDTH, Y * Game::BLOCK_HEIGHT, 0 );
		glBindTexture( GL_TEXTURE_2D, Game::nSmallDotTexture );
		glCallList( Object::m_DisplayList );
	glPopMatrix( );
	glPopAttrib( );
}

void SmallDot::update( )
{
	// do nothing
}