#pragma once
#ifndef _EMPTY_H_
#define _EMPTY_H_
#include "object.h"

class Empty : public Object
{

public:
	Empty( );
	~Empty( );

	void render( );
	void update( );
};



inline void Empty::render( )
{
	// do nothing
}

inline void Empty::update( )
{
	// do nothing
}


#endif