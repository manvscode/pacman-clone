#pragma once
#ifndef _PINKGHOST_H_
#define _PINKGHOST_H_
#include "Ghost.h"



class PinkGhost : public Ghost
{
  protected:

  public:
	PinkGhost( );
	~PinkGhost( );

	void render( );
	void update( );
};

#endif