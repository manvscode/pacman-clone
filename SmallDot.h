#pragma once
#include "object.h"

class SmallDot : public Object
{
  public:
	SmallDot( );
	~SmallDot( );

	void render( );
	void update( );
};
