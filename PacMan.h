#pragma once
#ifndef _PACMAN_H_
#define _PACMAN_H_
#include "object.h"

class PacMan : public Object
{
  protected:
	unsigned int animationPosition;

  public:
	PacMan( );
	~PacMan( );

	void render( );
	void update( );

	void moveUp( );	
	void moveDown( );
	void moveLeft( );
	void moveRight( );


	static bool isCollision( int x, int y );
};


#endif