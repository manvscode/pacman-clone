#include <cassert>
#include "OrangeGhost.h"
#include "PacManGame.h"


OrangeGhost::OrangeGhost( )
 : Ghost(Game::ORANGE_GHOST)
{
	type = OBJ_GHOST;

	orientation = (Object::Orientation) ( rand( ) % 4 );
}

OrangeGhost::~OrangeGhost( )
{
}

void OrangeGhost::render( )
{
	Ghost::render( Game::nOrangeGhostTexture );
}

void OrangeGhost::update( )
{
	static unsigned int nGhostUpdateCounter = 0;

	if( nGhostUpdateCounter % 4 == 0 )
	{
		switch( getState( ) )
		{
			case Ghost::SEEKING_PACMAN:
				seek( );
				break;
			case Ghost::FLEEING_PACMAN:
				flee( );
				break;
			case Ghost::DEAD:
				returnToHome( );
				break;
			default:
				break;
		}

		nGhostUpdateCounter = 0;
	}
	nGhostUpdateCounter++;
}

void OrangeGhost::seek( )
{
	Object *pPath = NULL;

	if( Game::pPlayer->isOrientation( PacMan::UP ) )
		pPath = _findPath( 0, 5 );		
	else if( Game::pPlayer->isOrientation( PacMan::DOWN ) )
		pPath = _findPath( 0, -5 );		
	else if( Game::pPlayer->isOrientation( PacMan::LEFT ) )
		pPath = _findPath( -5, 0 );		
	else // PacMan::RIGHT
		pPath = _findPath( 5, 0 );	

	if( pPath != NULL )
	{
		if( !isCollision( pPath, this ) )
		{
			X = pPath->X;
			Y = pPath->Y;
		}

		_pathCleanup( );
	}
}