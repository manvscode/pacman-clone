#pragma once
#ifndef _OBJECT_H_
#define _OBJECT_H_
#include <GL/glut.h>
#include <GL/gl.h>
#include <cmath>

class Object
{
  public:
	typedef enum tagOrientation { UP = 0, DOWN, LEFT, RIGHT, START } Orientation;

	enum ObjectType {
		OBJ_NONE = 0,
		OBJ_BLOCK,
		OBJ_SMALL_DOT,
		OBJ_BIG_DOT,
		OBJ_GHOST,
		OBJ_PACMAN,
		OBJ_EMPTY
	};

  protected:
	static unsigned int m_DisplayList;
	Orientation orientation; // 0, 1, 2, 3

  public:
	Object( );
	virtual ~Object( );

	virtual void render( ) = 0;
	virtual void update( ) = 0;

	//virtual void moveUp( ) = 0;	
	//virtual void moveDown( ) = 0;
	//virtual void moveLeft( ) = 0;
	//virtual void moveRight( ) = 0;

	
	static void initialize( );
	static void deinitialize( );

	//unsigned int distance( const Object *pObj1,  const Object *pObj2 );
	//unsigned int manhattanDistance( const Object *pObj1,  const Object *pObj2 );
	//unsigned int euclideanDistance( const Object *pObj1,  const Object *pObj2 );


	void setOrientation( Orientation o );
	bool isOrientation( Orientation o );

	int X, Y;
	ObjectType type;	
};

//
//inline unsigned int Object::distance( const Object *pObj1,  const Object *pObj2 )
//{ return manhattanDistance( pObj1, pObj2 ); }
//
//inline unsigned int Object::manhattanDistance( const Object *pObj1,  const Object *pObj2 )
//{ return (unsigned int) std::abs( (int) pObj1->X - pObj2->X ) + std::abs( (int) pObj1->Y - pObj2->Y ); }
//
//inline unsigned int Object::euclideanDistance( const Object *pObj1,  const Object *pObj2 )
//{ return (unsigned int) std::sqrt( (pObj1->X - pObj2->X)*(pObj1->X - pObj2->X) + (pObj1->Y - pObj2->Y)*(pObj1->Y - pObj2->Y) ); }



#endif