#pragma once
#include "object.h"

class BigDot : public Object
{
  protected:
	unsigned int animationPosition;

  public:
	BigDot( );
	~BigDot( );

	void render( );
	void update( );
};
