#ifndef _GHOSTPATHNODE_H_
#define _GHOSTPATHNODE_H_


class GhostPathNode
{
public:
	unsigned int distance;
	Object *pTile;


	GhostPathNode( unsigned int _distance, Object *&tile )
		: distance(_distance), pTile(tile)
	{}
};

inline bool operator<( const GhostPathNode &n1, const GhostPathNode &n2 )
{ 
	return n1.distance > n2.distance; 
}


#endif