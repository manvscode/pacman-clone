pacman-clone
============

This is a simple Pac Man clone that was written as part of a talk I did for GISIG group at Florida International University in 2008. It uses OpenGL for the graphics and a best-first seach for the path finding. 