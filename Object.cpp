#include "Object.h"
#include "PacManGame.h"


Object::Object( )
: X(0), Y(0), type(OBJ_BLOCK)
{
}

Object::~Object( )
{
}

unsigned int Object::m_DisplayList = 0;

void Object::initialize( )
{
	m_DisplayList = glGenLists( 1 );
	glNewList( m_DisplayList, GL_COMPILE );
		glBegin( GL_QUADS );
			glTexCoord2i( 0, 0 ); glVertex2i( 0, 0 );
			glTexCoord2i( 1, 0 ); glVertex2i( Game::BLOCK_WIDTH, 0 );
			glTexCoord2i( 1, 1 ); glVertex2i( Game::BLOCK_WIDTH, Game::BLOCK_HEIGHT );
			glTexCoord2i( 0, 1 ); glVertex2i( 0, Game::BLOCK_HEIGHT );
		glEnd( );
	glEndList( );
}

void Object::deinitialize( )
{
	glDeleteLists( m_DisplayList, 1 );
}
void Object::setOrientation( Orientation o )
{ orientation = o; }

bool Object::isOrientation( Orientation o )
{ return orientation == o; }

