#pragma
#ifndef _GHOST_AI_H_
#define _GHOST_AI_H_



class HFunction
{
  public:

	int operator()( const Object *pStartObject, const Object *pEndObject ) const
	{ return 10 * (abs( pStartObject->X - pEndObject->X) + abs( pStartObject->Y - pEndObject->Y ) ); }
};

class GFunction
{
  public:
	int operator()( const Object *pStartObject, const Object *pEndObject ) const
	{ /*return abs( pStartObject->X - pEndObject->X) + abs( pStartObject->Y - pEndObject->X );*/ 
	
		if( abs( pStartObject->X - pEndObject->X) > 0 && abs( pStartObject->Y - pEndObject->Y) > 0 )
			return 14;
		else
			return 10;
	
	}
};


#endif