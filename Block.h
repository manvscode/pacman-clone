#pragma once
#include "object.h"

class Block : public Object
{

public:
	Block( );
	~Block( );

	void render( );
	void update( );
};
