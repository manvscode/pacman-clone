#pragma once
#ifndef _ORANGEGHOST_H_
#define _ORANGEGHOST_H_
#include "ghost.h"



class OrangeGhost : public Ghost
{
  protected:

  public:
	OrangeGhost( );
	~OrangeGhost( );

	void render( );
	void update( );
	void seek( );
};

#endif