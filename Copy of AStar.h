#pragma once
#include <deque>
#include <stack>
#include <set>
#include <algorithm>
#include <functional>
#include <cassert>
#include "PacManClone.h"
#include "Object.h"


using namespace std;

template <class H, class G>
class F
{
  public:
	template <class T>
	int operator()( const T &t1,  const T &t2 ) const
	{ return H(t1, t2) + G(t1, t2); }
};


class AStarNode {
  public:
	Object *node;
	int f;
	int h;
	int g;
	AStarNode *pParent;

	bool operator==( const AStarNode &right ) const
	{ return node == right.node && f == right.f && h == right.h && g == right.g && pParent == right.pParent; }
};

bool operator==( AStarNode &left, AStarNode &right )
{ return left.node == right.node && 
		 left.f == right.f && 
		 left.h == right.h && 
		 left.g == right.g && 
		 left.pParent == right.pParent; }

bool operator==( const AStarNode &left, const AStarNode &right )
{ return left.node == right.node && 
		 left.f == right.f && 
		 left.h == right.h && 
		 left.g == right.g && 
		 left.pParent == right.pParent; }

class less_AStar_f 
{
  public:
	bool operator()( const AStarNode &a1, const AStarNode &a2 ) const
	{ return a1.f < a2.f; }
};
	

template <class H, class G>
class AStar
{
  public:	
	typedef std::stack<Object *> PathCollection;

  protected:
	H _H;
	G _G;

	typedef std::deque<AStarNode> OpenCollection;
	typedef std::set<Object *> ClosedCollection;

	OpenCollection openList;
	ClosedCollection closeList;
	PathCollection shortestPath;
	bool bPathFound;
	Object *_pEndNode;

  public:
	AStar( )
		: openList( ), closeList( ), bPathFound(false), _pEndNode(0)
	{
	}

	~AStar( )
	{
	}

   //1.  Add the starting square (or node) to the open list.
   //2. Repeat the following:
   //   a) Look for the lowest F cost square on the open list. We refer to this as the current square.
   //   b) Switch it to the closed list.
   //   c) For each of the 8 squares adjacent to this current square �
   //       * If it is not walkable or if it is on the closed list, ignore it. Otherwise do the following.
   //       * If it isn't on the open list, add it to the open list. Make the current square the parent of this square. Record the F, G, and H costs of the square.
   //       * If it is on the open list already, check to see if this path to that square is better, using G cost as the measure. A lower G cost means that this is a better path. 
   //	      If so, change the parent of the square to the current square, and recalculate the G and F scores of the square. If you are keeping your open list sorted by F score, 
   //		  you may need to resort the list to account for the change.

   //   d) Stop when you:
   //       * Add the target square to the closed list, in which case the path has been found (see note below), or
   //       * Fail to find the target square, and the open list is empty. In this case, there is no path.
   //3. Save the path. Working backwards from the target square, go from each square to its parent square until you reach the starting square. That is your path. 
	bool computeShortestPath( Object *pStartNode, Object *pEndNode )
	{
		_pEndNode = pEndNode;
		openList.clear( );
		closeList.clear( );
		less_AStar_f less;

		make_heap( openList.begin( ), openList.end( ), less ); // create a binary min heap

		openList.push_back( buildNode( pStartNode, NULL ) );
		push_heap( openList.begin( ), openList.end( ), less ); // push onto a binary min heap...

		bPathFound = false;

		while( openList.size( ) > 0 && !bPathFound )
		{
			AStarNode &current = openList.front( );
			openList.pop_front( );

			if( current.node == pEndNode )
			{
				bPathFound = true;

				if( shortestPath.size( ) > 0 ) shortestPath.pop( );

				AStarNode *pNode = &current;

				while( pNode->pParent != NULL )
				{
					shortestPath.push( pNode->node );
					pNode = pNode->pParent;
				}
			}

			for( int i = -1; i <= 1; i++ )
			{
				for( int j = -1; j <= 1; j++ )
				{
					int x = current.node->X + j;
					int y = current.node->Y + i;

					if( x < 0 ) continue;
					if( y < 0 ) continue;
					if( x > Game::GAME_BOARD_SIZE ) continue;
					if( y > Game::GAME_BOARD_SIZE ) continue;

					Object *pObj = Game::gameBoard[ y * Game::GAME_BOARD_SIZE + x ];

					if( pObj->type == Object::OBJ_BLOCK ) continue; // can't walk through blocks...
					if( closeList.find( pObj ) != closeList.end( ) ) continue; // its in the close list...


					OpenCollection::iterator itr = find( openList.begin( ), openList.end( ), pObj );

					if( itr == openList.end( ) )
					{
						openList.push_back( buildNode( pObj, &current ) );
						push_heap( openList.begin( ), openList.end( ), less ); // push onto a binary min heap...
					}
					else // already on the open list
					{
						int g = current.g + _G(current.node, pObj);

						if( g < itr->g )
						{
							itr->pParent = &current;
							itr->g = g;
							itr->h = _H( pObj, pEndNode );
							itr->f = itr->g + itr->h;

							 sort_heap( openList.begin( ), openList.end( ), less ); // create a binary min heap
						}
					}

				}
			}
		}

		return bPathFound;
	}

	AStarNode buildNode( Object *pObj, AStarNode *parent )
	{
		AStarNode node;
		node.h = _H( pObj, _pEndNode );
		node.g = parent != NULL ? _G(pObj, parent->node) : 0;
		node.f = node.h + node.g;
		node.pParent = parent;

		return node;
	}

	PathCollection getShortestPath( )
	{
		assert( bPathFound ); // you need to call computeShortestPath( );
		return shortestPath;
	}

};
