#include "BigDot.h"
#include "PacManGame.h"

BigDot::BigDot( )
{
	type = OBJ_BIG_DOT;
}

BigDot::~BigDot( )
{
}

void BigDot::render( )
{
	glPushAttrib( GL_TEXTURE_BIT );

	glPushMatrix( );
		glMatrixMode( GL_TEXTURE );
		glPushMatrix( );
		glLoadIdentity( );
		glScalef( 1.0f / 2.0f, 1.0f, 1.0f );
		glTranslatef( animationPosition, 0, 0 );
		glMatrixMode( GL_MODELVIEW );

		glTranslatef( X * Game::BLOCK_WIDTH, Y * Game::BLOCK_HEIGHT, 0 );
		glBindTexture( GL_TEXTURE_2D, Game::nBigDotTexture );
		glCallList( Object::m_DisplayList );

		glMatrixMode( GL_TEXTURE );
		glPopMatrix( );
		glMatrixMode( GL_MODELVIEW );
	glPopMatrix( );
	glPopAttrib( );
}

void BigDot::update( )
{
	animationPosition = animationPosition == 1 ? 0 : 1;
}