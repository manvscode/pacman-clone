#include <cassert>
#include <queue>
#include <utility>
#include "RedGhost.h"
#include "PacManGame.h"
#include "GhostPathNode.h"


RedGhost::RedGhost( )
: Ghost( Game::RED_GHOST ) 
{
	type = OBJ_GHOST;

	orientation = Object::LEFT;//(Object::Orientation) ( rand( ) % 4 );
}

RedGhost::~RedGhost( )
{
}

void RedGhost::render( )
{
	Ghost::render( Game::nRedGhostTexture );
}

void RedGhost::update( )
{
	static unsigned int nGhostUpdateCounter = 0;

	if( nGhostUpdateCounter % 3 == 0 )
	{
		switch( getState( ) )
		{
			case Ghost::SEEKING_PACMAN:
				seek( );
				break;
			case Ghost::FLEEING_PACMAN:
				flee( );
				break;
			case Ghost::DEAD:
				returnToHome( );
				break;
			default:
				break;
		}

		nGhostUpdateCounter = 0;
	}
	nGhostUpdateCounter++;
}


