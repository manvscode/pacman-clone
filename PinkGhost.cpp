#include <cassert>
#include <queue>
#include <utility>
#include "PinkGhost.h"
#include "PacManGame.h"


PinkGhost::PinkGhost( )
: Ghost( Game::PINK_GHOST )
{
	type = OBJ_GHOST;

	orientation = (Object::Orientation) ( rand( ) % 4 );
}

PinkGhost::~PinkGhost( )
{
}

void PinkGhost::render( )
{
	Ghost::render( Game::nPinkGhostTexture );
}

void PinkGhost::update( )
{
	static unsigned int nGhostUpdateCounter = 0;

	if( nGhostUpdateCounter % 4 == 0 )
	{
		
		switch( getState( ) )
		{
			case Ghost::SEEKING_PACMAN:
				{
					if( rand( ) % 6 == 0 )
					{
						seek( );
					}
					else // random direction
					{
						ObjectSuccessors getSuccessors;
						vector<Object *> &successors = getSuccessors( this );
						int randomIndex = rand( ) % successors.size( );

						if( successors[ randomIndex ]->type != Object::OBJ_BLOCK )
						{
							X = successors[ randomIndex ]->X;
							Y = successors[ randomIndex ]->Y;
						}
					}
					break;
				}
			case Ghost::FLEEING_PACMAN:
				{
					flee( );
					break;
				}
			case Ghost::DEAD:
				{
					returnToHome( );
					break;
				}
			default:
				break;
		}

		nGhostUpdateCounter = 0;
	}
	nGhostUpdateCounter++;

}

