#pragma once
#ifndef _CYANGHOST_H_
#define _CYANGHOST_H_
#include "ghost.h"



class CyanGhost : public Ghost
{
  protected:

  public:
	CyanGhost( );
	~CyanGhost( );

	void render( );
	void update( );
	void seek( );
};

#endif